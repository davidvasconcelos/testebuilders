package br.com.builders.cliente.controller;

import br.com.builders.cliente.dto.NomeClienteDto;
import br.com.builders.cliente.model.Cliente;
import br.com.builders.cliente.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/clientes")
@Api(value = "Cliente", description = "Cliente REST API")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @ApiOperation(httpMethod = "GET", value = "Método get para pesquisar um cliente por id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o cliente pesquisado", response = Cliente.class),
            @ApiResponse(code = 409, message = "Informa que o cliente não existe", response = Cliente.class)
    })
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> buscarPorId(@PathVariable Long id) {

        final Cliente cliente = clienteService.buscarClientePorId(id);

        return ResponseEntity.ok(cliente);
    }

    @ApiOperation(httpMethod = "GET", value = "Método get para pesquisar um cliente por cpf e nome")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o cliente pesquisado", response = Cliente.class),
            @ApiResponse(code = 409, message = "Informa que o cliente não existe", response = Cliente.class)
    })
    @GetMapping
    public ResponseEntity<Page<Cliente>> buscarPorParametros(@RequestParam(defaultValue = "0") Integer page,
                                                             @RequestParam(defaultValue = "10") Integer size,
                                                             @RequestParam(defaultValue = "") Long cpf,
                                                             @RequestParam(defaultValue = "") String nome) {

        final Pageable pageable = PageRequest.of(page, size);

        final Page<Cliente> clientes = clienteService.buscarPorParametros(cpf, nome, pageable);

        return ResponseEntity.ok(clientes);
    }

    @ApiOperation(httpMethod = "POST", value = "Método post inserir um cliente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna o path para pesquisa")})
    @PostMapping
    public ResponseEntity criarCliente(@Valid @RequestBody Cliente cliente) {

        final Cliente clienteSalvo = clienteService.salvarCliente(cliente);

        URI location = getUri(clienteSalvo);

        return ResponseEntity.created(location).build();

    }

    @ApiOperation(httpMethod = "PUT", value = "Método put para alterar um cliente por id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso", response = Cliente.class)})
    @PutMapping("/{id}")
    public ResponseEntity atualizarCliente(@PathVariable Long id, @Valid @RequestBody Cliente cliente) {

        final Cliente clienteAtualizado = clienteService.atualizarCliente(id, cliente);

        return ResponseEntity.ok(clienteAtualizado);
    }

    @ApiOperation(httpMethod = "PATCH", value = "Método patch para alterar o nome de um cliente por id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso", response = Cliente.class)})
    @PatchMapping("/{id}")
    public ResponseEntity partialUpdateGeneric( @PathVariable("id") Long id,
                                                @Valid @RequestBody NomeClienteDto nome) {

        final Cliente clienteSalvo = clienteService.atualizarNomeDoCliente(id, nome);

        return ResponseEntity.ok(clienteSalvo);
    }

    @ApiOperation(httpMethod = "DELETE", value = "Método put para deletar um cliente por id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Sucesso")})
    @DeleteMapping("/{id}")
    public ResponseEntity deletarCliente(@PathVariable Long id) {

        clienteService.removerCliente(id);

        return ResponseEntity.noContent().build();
    }


    private URI getUri(Cliente cliente) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(cliente.getId()).toUri();
    }
}
