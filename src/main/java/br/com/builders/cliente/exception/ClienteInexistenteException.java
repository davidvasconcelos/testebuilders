package br.com.builders.cliente.exception;

public class ClienteInexistenteException extends RuntimeException {

    private static final long serialVersionUID = 2572033973588885747L;

    public ClienteInexistenteException() {
        super();
    }

    public ClienteInexistenteException(final String message) {
        super(message);
    }


}