package br.com.builders.cliente.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cliente implements Serializable {

    private static final long serialVersionUID = 2244824415642317845L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "ID gerado automaticamente")
    private Long id;

    @NotEmpty(message = "Nome é obrigatório")
    @ApiModelProperty(notes = "Nome do cliente", required = true)
    private String nome;

    @NotNull(message = "CPF é obrigatório")
    @ApiModelProperty(notes = "Cpf do cliente", required = true)
    private Long cpf;

    @NotNull(message = "Data de nascimento é obrigatória")
    @ApiModelProperty(notes = "Data de nascimento do cliente", required = true)
    @JsonFormat(pattern = "dd/MM/yyyy",
            shape = JsonFormat.Shape.STRING,
            locale = "pt-BR",
            timezone = "America/Sao_Paulo")
    private LocalDate dataNascimento;

    @Transient
    @ApiModelProperty(notes = "Idade do cliente calculada automaticamente", readOnly = true)
    private Long idade;

    public Long getIdade() {
        return ChronoUnit.YEARS.between(Optional.ofNullable(dataNascimento).orElse(LocalDate.now()), LocalDate.now());
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf=" + cpf +
                ", dataNascimento=" + dataNascimento +
                ", idade=" + idade +
                '}';
    }
}
