package br.com.builders.cliente.service;

import br.com.builders.cliente.dto.NomeClienteDto;
import br.com.builders.cliente.exception.ClienteInexistenteException;
import br.com.builders.cliente.model.Cliente;
import br.com.builders.cliente.repository.ClienteRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente buscarClientePorId(final Long id) {

        final Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        final Cliente cliente = optionalCliente.orElseThrow(() -> new ClienteInexistenteException("Cliente não encontrado"));

        return cliente;

    }

    public Page<Cliente> buscarPorParametros(Long cpf, String nome, Pageable pageable) {

        return clienteRepository.findByCpfOrNomeContaining(cpf, nome, pageable);
    }

    public Cliente salvarCliente(final Cliente cliente) {

        return clienteRepository.save(cliente);

    }

    public Cliente atualizarCliente(final Long id, final Cliente cliente) {

        final Cliente clienteSalvo = buscarClientePorId(id);

        BeanUtils.copyProperties(cliente, clienteSalvo, "id");

        return clienteRepository.save(clienteSalvo);

    }

    public Cliente atualizarNomeDoCliente(final Long id, final NomeClienteDto nome) {

        final Cliente clienteSalvo = buscarClientePorId(id);

        clienteSalvo.setNome(nome.getNome());

        return clienteRepository.save(clienteSalvo);

    }

    public void removerCliente(final Long id) {

        final Cliente clienteSalvo = buscarClientePorId(id);

        clienteRepository.delete(clienteSalvo);
    }


}
