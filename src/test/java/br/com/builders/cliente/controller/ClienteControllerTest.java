package br.com.builders.cliente.controller;

import br.com.builders.cliente.dto.NomeClienteDto;
import br.com.builders.cliente.exception.ClienteInexistenteException;
import br.com.builders.cliente.exception.ErrorResponse;
import br.com.builders.cliente.model.Cliente;
import br.com.builders.cliente.service.ClienteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ClienteController.class)
public class ClienteControllerTest {

    private ObjectMapper mapper;

    private Gson gson;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClienteService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        gson = new Gson();
    }

    @Test
    public void deveTrazerUmClientePeloId() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(1L, "Cliente 1", 54585950842L,
                LocalDate.of(1985, Month.JULY, 30), null);
        final Long id = 1L;

        final String clienteJson = mapper.writeValueAsString(cliente);

        when(this.service.buscarClientePorId(id)).thenReturn(cliente);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.get("/clientes/{id}", id))
                //verificao
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(clienteJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void deveTratarClienteInexistente() throws Exception {

        //cenario
        final Long id = 1L;

        final ErrorResponse errorResponse = new ErrorResponse();
        final List<String> detalhes = Arrays.asList("Cliente não encontrado");
        errorResponse.setDetalhes(detalhes);
        errorResponse.setMensagem("Conflito");

        final String errorJson = mapper.writeValueAsString(errorResponse);

        when(this.service.buscarClientePorId(id)).thenThrow(new ClienteInexistenteException("Cliente não encontrado"));

        //acao
        mockMvc.perform(MockMvcRequestBuilders.get("/clientes/{id}", id))
                //verificacao
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(errorJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
        ;
    }


    @Test
    public void deveTratarNomeNulo() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(1L, null, 54585950842L,
                LocalDate.of(1985, Month.JULY, 30), null);

        final String clienteJson = mapper.writeValueAsString(cliente);

        final ErrorResponse errorResponse = new ErrorResponse();
        final List<String> detalhes = Arrays.asList("Nome é obrigatório");
        errorResponse.setDetalhes(detalhes);
        errorResponse.setMensagem("Falha na validação");

        final String errorJson = mapper.writeValueAsString(errorResponse);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .content(clienteJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //verificacao
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(errorJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void deveTratarCPFNulo() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(1L, "Cliente 1", null,
                LocalDate.of(1985, Month.JULY, 30), null);

        final String clienteJson = mapper.writeValueAsString(cliente);

        final ErrorResponse errorResponse = new ErrorResponse();
        final List<String> detalhes = Arrays.asList("CPF é obrigatório");
        errorResponse.setDetalhes(detalhes);
        errorResponse.setMensagem("Falha na validação");

        final String errorJson = mapper.writeValueAsString(errorResponse);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .content(clienteJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //verificacao
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(errorJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void deveTratarDataNascimentoNulo() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(1L, "Cliente 1", 54585950842L,
                null, null);

        final String clienteJson = mapper.writeValueAsString(cliente);

        final ErrorResponse errorResponse = new ErrorResponse();
        final List<String> detalhes = Arrays.asList("Data de nascimento é obrigatória");
        errorResponse.setDetalhes(detalhes);
        errorResponse.setMensagem("Falha na validação");

        final String errorJson = mapper.writeValueAsString(errorResponse);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .content(clienteJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //verificacao
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(errorJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void deveTratarTodosCamposNulos() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(null,null,null,null,null);

        final String clienteJson = mapper.writeValueAsString(cliente);

        final ErrorResponse errorResponse = new ErrorResponse();
        final List<String> detalhes = Arrays.asList("Nome é obrigatório","CPF é obrigatório","Data de nascimento é obrigatória");
        errorResponse.setDetalhes(detalhes);
        errorResponse.setMensagem("Falha na validação");

        final String errorJson = mapper.writeValueAsString(errorResponse);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .content(clienteJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //verificacao
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(errorJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }


    @Test
    public void deveRetornarStatusCreated() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(1L, "Cliente 1", 54585950842L,
                LocalDate.of(1985, Month.JULY, 30), null);

        final String clienteJson = mapper.writeValueAsString(cliente);

        when(this.service.salvarCliente(cliente)).thenReturn(cliente);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(clienteJson)
                .characterEncoding("utf-8"))
                //verificao
                .andExpect(status().isCreated());
    }

    @Test
    public void deveRetornarStatusOKComClienteAlterado() throws Exception {

        //cenario
        final Cliente cliente = new Cliente(1L, "Cliente 1", 54585950842L,
                LocalDate.of(1985, Month.JULY, 30), null);

        final Cliente clienteAlterado = new Cliente(1L, "Cliente alterado", 54585950842L,
                LocalDate.of(1985, Month.JULY, 30), null);
        final Long id = 1L;

        final String clienteJson = mapper.writeValueAsString(cliente);
        final String clienteAlteradoJson = mapper.writeValueAsString(clienteAlterado);

        when(this.service.atualizarCliente(id, cliente)).thenReturn(clienteAlterado);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.put("/clientes/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(clienteJson)
                .characterEncoding("utf-8"))
                //verificao
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(clienteAlteradoJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void deveTratarNomeNuloNaAlteracaoDeNome() throws Exception {

        //cenario
        final NomeClienteDto nome = new NomeClienteDto(null);

        final String nomeJson = mapper.writeValueAsString(nome);

        final ErrorResponse errorResponse = new ErrorResponse();
        final List<String> detalhes = Arrays.asList("Nome é obrigatório");
        errorResponse.setDetalhes(detalhes);
        errorResponse.setMensagem("Falha na validação");

        final String errorJson = mapper.writeValueAsString(errorResponse);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.patch("/clientes/1")
                .content(nomeJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //verificacao
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(errorJson))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void deveRetornarStatusNoContent() throws Exception {

        //cenario
        final Long id = 1L;

        doNothing().when(service).removerCliente(id);

        //acao
        mockMvc.perform(MockMvcRequestBuilders.delete("/clientes/{id}", id))
                //verificao
                .andExpect(status().isNoContent());
    }


}
